# 智橙预装 APP 列表

## 系统应用

这些请安装在 /system/app 下：

CarLauncher.apk   
DrivingRecorder.apk   
VoiceNow.apk   


## 第三方应用

这些是需要安装的第三方APP，有些是临时选用的，不一定会预装到最终产品里去。所以不一定要安装到 /system/app 下，普通安装也可。

应用名称 | APK 文件名 |
:-----: | :-------: |
百度地图 | BaiduMaps_Android.apk
优驾OBD | gooddriver2.9.8.3.apk
路况电台 | LukuangFM-138.apk
音乐雷达 | MRadarLand.apk
天天动听 | TTPod-Android-v7.8.1_H1.apk
高德地图 | amap_android.apk
自驾助手 | zijiazhushou_android_car.apk
胎压检测 | TPMS--APP.apk
微信 | weixin610android540.apk
FM电台 | FMLaunch.apk
